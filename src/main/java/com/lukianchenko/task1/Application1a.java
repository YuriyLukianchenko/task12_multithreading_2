package com.lukianchenko.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Application1a {
    Lock lock = new ReentrantLock();
    Logger logger = LogManager.getLogger(Application1a.class);
    public  void method1(){
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " this is method 1");
            }
        } finally {
            lock.unlock();
        }
    }
    public void method2(){
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " this is method 2");
            }
        } finally {
            lock.unlock();
        }
    }
    public void method3(){
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " this is method 3");
            }
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Application1.class);
        Application1a app = new Application1a();
        Application1a app1 = new Application1a();
        Application1a app2 = new Application1a();


        // different app objects has different objects lock, so we have three different locks
        Thread thread1 = new Thread(()-> app.method1());
        Thread thread2 = new Thread(()-> app1.method2());
        Thread thread3 = new Thread(()-> app2.method3());

        thread1.start();
        thread2.start();
        thread3.start();


        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            logger.info("Interrupt");
        }


        logger.info("main thread end------------------------");
    }

}
