package com.lukianchenko.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Application1 {
    Lock lock = new ReentrantLock();
    Logger logger = LogManager.getLogger(Application1.class);
    public  void method1(){
        lock.lock();
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 1");
        }
        lock.unlock();
    }
    public void method2(){
        lock.lock();
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 2");
        }
        lock.unlock();
    }
    public void method3(){
        lock.lock();
        for(int i = 0; i < 1000; i++) {
            System.out.println(i + " this is method 3");
        }
        lock.unlock();
    }

    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Application1.class);
        Application1 app = new Application1();


        Thread thread1 = new Thread(()-> app.method1());
        Thread thread2 = new Thread(()-> app.method2());
        Thread thread3 = new Thread(()-> app.method3());

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            logger.info("Interrupt");
        }
    }
}
