package com.lukianchenko.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueueTesting {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(BlockingQueueTesting.class);
        BlockingQueue<String> queue = new LinkedTransferQueue<>(); //one take one put
        BlockingQueue<String> queue2 = new PriorityBlockingQueue<>(); //all put all take ?

        // just por getting practicing with using stream (stupid practice)
        new Thread(() -> {
                new LinkedList<String>(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9")).stream()
                        .forEach( (x) -> {
                            try {
                                logger.info(queue.take());

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        });

        }).start();
        new Thread(() -> {
            for(int i = 1; i < 10; i++) {
                try {
                    queue.put("Bobson" + i);
                    queue.put("Bob" + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
